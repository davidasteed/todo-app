import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
import TodoApp from "./components/todo/TodoApp";
import "./bootstrap.css";

class App extends Component {
  render() {
    return (
      <div className = "App">
        <TodoApp />
      </div>
    )
  };
}

// class LearningComponents extends Component {
//   render() {
//     return (
//       <div className="LearningComponents">
//         <p>hello</p>
//       </div>
//     );
//   }
// }

export default App;
