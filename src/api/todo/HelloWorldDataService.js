import axios from "axios";

class HelloWorldDataService {
  executeHelloWorldService() {

    return axios.get("http://localhost:8080/hello-world");
  }

  executeHelloWorldBeanService() {

    return axios.get("http://localhost:8080/hello-world-bean");
  }

  executeHelloWorldPathVariableService(name) {
    // now taken care of by the interceptor
    // let username = "in28minutes";
    // let password = "dummy";

    // let basicAuthHeader = "Basic " + window.btoa(username + ":" + password);

    return axios.get(`http://localhost:8080/hello-world/path-variable/${name}`
    // ,
    //   {
    //     authorization: basicAuthHeader
    //   }
    );
  }
}

export default new HelloWorldDataService();