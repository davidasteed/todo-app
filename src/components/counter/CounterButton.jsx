import React, { Component } from "react";
import PropTypes from "prop-types";

class CounterButton extends Component {
  render = () => (
    <div>
      <button className="CounterButton" onClick={() => this.props.incrementParent(this.props.by)}>+{this.props.by}</button>
      <button className="CounterButton" onClick={() => this.props.decrementParent(this.props.by)}>-{this.props.by}</button>
    </div>
  );
};

CounterButton.defaultProps = { by: 1 };
CounterButton.propTypes = { by: PropTypes.number };

export default CounterButton;