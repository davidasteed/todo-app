import React, { Component } from "react";
import "./Counter.css";
import CounterButton from "./CounterButton";

class Counter extends Component {
  constructor() {
    super();
    this.state = {
      counter: 0
    };

    // traditional React uses bind()
    // only need to do this if render() or increment() is *not* an arrow function
    // (arrow functions don't have a "this" object)
    this.incrementParent = this.incrementParent.bind(this);
    this.decrementParent = this.decrementParent.bind(this);
  }

  render() {
    return (
      <>
        <div>
          <h2>Counter</h2>
        </div>
        <div>
          <CounterButton by={100} incrementParent={this.incrementParent} decrementParent={this.decrementParent} counter={this.state.counter} />
          <CounterButton by={10} incrementParent={this.incrementParent} decrementParent={this.decrementParent} counter={this.state.counter} />
          <CounterButton by={5} incrementParent={this.incrementParent} decrementParent={this.decrementParent} counter={this.state.counter}></CounterButton>
          <CounterButton incrementParent={this.incrementParent} decrementParent={this.decrementParent} counter={this.state.counter}></CounterButton>
          <span className="count">{this.state.counter}</span>
          <div>
            <button className="reset" onClick={this.reset}>Reset</button>
          </div>
        </div>
      </>
    );
  }

  incrementParent(by) {
    console.log(`increment called from parent with: ${by}`);
    // this.state.counter++; // bad practice to modify state directly, instead:
    this.setState({
      counter: this.state.counter + by
    }, () => console.log('parent:  counter value is now: ', this.state.counter));
  }

  decrementParent(by) {
    console.log(`decrement called from parent with: ${by}`);
    this.setState({
      counter: this.state.counter - by
    }, () => console.log('parent:  counter value is now: ', this.state.counter));
  }

  reset = () => this.setState({ counter: 0 });
}

export default Counter;
