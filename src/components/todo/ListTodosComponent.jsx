import React, { Component } from "react";
import moment from "moment";
import TodoDataService from "../../api/todo/TodoDataService.js";
import AuthenticationService from "./AuthenticationService.js";

class ListTodosComponent extends Component {
  constructor(props) {
    console.log("ListTodosComponent constructor");

    super(props);
    this.state = {
      todos: [],
      message: null
    };
    this.updateTodoClicked = this.updateTodoClicked.bind(this);
  }

  /*
    best practice in React:  don't hold up component creation,
    and instead do any state/fetch/etc. only after the component has mounted

    order that methods run when a component is created in the UI (see in console logs):

      ListTodosComponent render
      ListTodosComponent.jsx:7 ListTodosComponent constructor
      ListTodosComponent.jsx:42 ListTodosComponent render
      ListTodosComponent.jsx:32 ListTodosComponent componentDidMount
      ListTodosComponent.jsx:42 ListTodosComponent render

    render() is first called on the initial state (defined in the constructor)

    setState() does not result in an immediate update of the state
  */

  componentWillUnmount() {
    // called when the component will be removed from the View
    console.log("ListTodosComponent componentWillUnmount");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("ListTodosComponent shouldComponentUpdate ");
    console.log("ListTodosComponent shouldComponentUpdate nextProps: ", nextProps);
    console.log("ListTodosComponent shouldComponentUpdate nextState: ", nextState);
    return true;   // or return false to inhibit the re-render() after componentDidMount()

    // by returning false:  we are telling React in essence:  even if the state is updated, do not update the View
    // i.e. we can skip rendering the view for, say, performance reasons, for a few state changes
  }

  componentDidMount() {
    console.log("ListTodosComponent componentDidMount");
    this.refreshTodos();

    // render() will be automatically called again upon return
  }

  render() {
    console.log("ListTodosComponent render");
    return (
      <div>
        <h1>List Todos</h1>
        {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
        <div className="container">
          <table className="table">
            <thead>
              <tr>
                <th>Description</th>
                <th>Target Date</th>
                <th>Is Completed?</th>
                <th>Update</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {/* <tr>
              <td>{this.state.todo.id}</td>
              <td>{this.state.todo.description}</td>
            </tr> */}

              {this.state.todos.map((todo) =>
                <tr key={todo.id}>
                  <td>{todo.description}</td>
                  <th>{moment(todo.targetDate).format("YYYY-MM-DD")}</th>
                  <th>{todo.done.toString()}</th>
                  <th><button className="btn btn-success" onClick={() => this.updateTodoClicked(todo.id)}>Update</button></th>
                  <th><button className="btn btn-warning" onClick={() => this.deleteTodoClicked(todo.id)}>Delete</button></th>
                </tr>
              )}
            </tbody>
          </table>
          <div className="row">
            <button className="btn btn-success" onClick={this.addTodoClicked}>Add</button>
          </div>
        </div>
      </div>
    )
  }

  deleteTodoClicked = (id) => {
    let username = AuthenticationService.getLoggedInUserName();
    TodoDataService.deleteTodo(username, id)
      .then((res) => {
        this.setState({ message: `Delete of todo ${id} successful` });
        this.refreshTodos();
      });
  };

  updateTodoClicked = (id) => {
    this.props.history.push(`/todos/${id}`);

    // let username = AuthenticationService.getLoggedInUserName();
    // todoDataService.updateTodo(username, id)
    //   .then((res) => {
    //     this.setState({ message: `Delete of todo ${id} successful` });
    //     this.refreshTodos();
    //   });
  };

  refreshTodos = () => {
    let username = AuthenticationService.getLoggedInUserName();
    TodoDataService.retrieveAllTodos(username)
      .then((response) => {
        // console.log(response);
        this.setState({ todos: response.data });
      })
  };

  addTodoClicked = () => {
    this.props.history.push(`/todos/-1`);
  };
}

export default ListTodosComponent;