import React, { Component } from 'react';

class FooterComponent extends Component {
  render() {
    return (
      <footer className="footer">
        <span className="text-muted">All rights reserved 2021 @http://www.soundcloud.com/davidasteed</span>
      </footer>
    )
  }
}

export default FooterComponent;