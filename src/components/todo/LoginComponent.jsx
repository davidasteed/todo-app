import React, { Component } from 'react';
import AuthenticationService from './AuthenticationService.js';

class LoginComponent extends Component {
  // create an initial state
  constructor(props) {
    super(props);   // best practice regarding props
    this.state = {
      username: "in28minutes",
      password: "",
      hasLoginFailed: false,
      showSuccessMessage: false
    }

    // only if not an arrow function:
    // this.handleChange = this.handleChange.bind(this);
  }
  render() {
    return (
      <div>
        <h1>Login</h1>
        <div className="container">
          {/* <div> */}
          {/* logical AND means we don't have to do custom functions */}
          {/* <ShowInvalidCredentials hasLoginFailed={this.state.hasLoginFailed} />
          <ShowSuccessMessage showSuccessMessage={this.state.showSuccessMessage} /> */}
          {this.state.hasLoginFailed && <span className="alert alert-warning" style={{ "display": "block" }}>Invalid credentials</span>}
          {/* {this.state.showSuccessMessage && <span>Login successful</span>} */}
        </div>
        <span>User Name: </span>
        <input
          type="text"
          name="username"
          value={this.state.username}
          onChange={this.handleChange}
        />
        <span>Password: </span>
        <input
          type="password"
          name="password"
          value={this.state.password}
          onChange={this.handleChange}
        />
        <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
        {/* </div> */}

      </div>

    )
  }

  // a "controlled component" in one in which changes are entirely dictated by the state
  // i.e. in a "controlled component" the state is the source of truth

  // in DOM this would be a "click event".  In react, it is a "synthetic event"
  // console.log("name event: ", event.target.value);

  // the function below can handle any number of changes from any number of components

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      [event.target.password]: event.target.password
    });
  };

  loginClicked = () => {
    // used before backend authentication api was created
    // if (this.state.username === "in28minutes" && this.state.password === "dummy") {
    //   // this.props.history.push(`/welcome/${this.state.username}, ${this.state.password}`);

    //   AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
    //   this.props.history.push(`/welcome/${this.state.username}`);

    //   // this.setState({
    //   //   showSuccessMessage: true,
    //   //   hasLoginFailed: false
    //   // });
    // } else {
    //   this.setState({
    //     showSuccessMessage: false,
    //     hasLoginFailed: true
    //   });
    // }

    // used with basic authentication
    // AuthenticationService
    //   .executeBasicAuthenticationService(this.state.username, this.state.password)
    //   .then(() => {
    //     AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
    //     this.props.history.push(`/welcome/${this.state.username}`);
    //   })
    //   .catch(() => {
    //     this.setState({
    //       showSuccessMessage: false,
    //       hasLoginFailed: true
    //     });
    //   })

    AuthenticationService
      .executeJwtAuthenticationService(this.state.username, this.state.password)
      .then((response) => {
        AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data.token);
        this.props.history.push(`/welcome/${this.state.username}`);
      })
      .catch(() => {
        this.setState({
          showSuccessMessage: false,
          hasLoginFailed: true
        });
      })
  };
}

// logical AND means we don't have to do custom functions
// function ShowInvalidCredentials(props) {
//   if (props.hasLoginFailed === true) {
//     return (
//       <span>Invalid credentials</span>
//     );
//   }

//   return null;
// };

// function ShowSuccessMessage(props) {
//   if (props.showSuccessMessage === true) {
//     return (
//       <span>Login successful</span>
//     );
//   }

//   return null;
// };

export default LoginComponent;