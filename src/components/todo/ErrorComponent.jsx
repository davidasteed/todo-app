function ErrorComponent() {
  return (
    <div>Oops!  An error occurred in the todo/counter applications and sent you to this page.</div>
  )
}

export default ErrorComponent;