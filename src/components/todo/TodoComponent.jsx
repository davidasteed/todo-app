import { ErrorMessage, Field, Form, Formik } from "formik";
import moment from "moment";
import React, { Component } from "react";
import AuthenticationService from "./AuthenticationService.js";
import TodoDataService from "../../api/todo/TodoDataService.js"

class TodoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: "",
      id: this.props.match.params.id,
      targetDate: moment(new Date()).format("YYYY-MM-DD")
    }
  }

  componentDidMount() {
    if (this.state.id === "-1") {
      return; // creating a new todo
    } else {
      let username = AuthenticationService.getLoggedInUserName();
      TodoDataService.retrieveTodo(username, this.state.id)
        .then((response) => {
          this.setState({
            description: response.data.description,
            targetDate: moment(response.data.targetDate).format("YYYY-MM-DD")
          });
        });
    }
  }

  onSubmit = (values) => {
    let username = AuthenticationService.getLoggedInUserName();
    let todo = {
      id: this.state.id,
      description: values.description,
      targetDate: values.targetDate,
      username: username
    };

    if (this.state.id === "-1") {
      TodoDataService.createTodo(username, todo)
        .then(() => this.props.history.push("/todos"));
    } else {
      TodoDataService.updateTodo(username, this.state.id, todo)
        .then(() => this.props.history.push("/todos"));
    }
  };

  validate = (values) => {
    let errors = {};
    if (!values.description) {
      errors.description = "Please enter a Description";
    } else if (values.description.length < 5) {
      errors.description = "Description must be at least 5 characters";
    }

    if (!moment(values.targetDate).isValid()) {
      errors.targetDate = "Please enter a valid Target Date";
    }
    return errors;
  };

  render() {
    let { description, targetDate } = this.state;
    return (
      <div>
        <h1>Todo</h1>
        <div className="container">
          {
          /* 
            1. shortcut notation below: instead of description: description 
              console:
              let name="in28minutes";
              let course="Full Stack";
              let details1={name, course};

              details1
              {name: 'in28minutes', course: 'Full Stack'}

            2. validate() is called before onSubmit()

            3. Formik by default enables validation onblur and onchange events

            4. Formik does not refresh forms by default:  enableReinitialize={false}
          */}
          <Formik
            enableReinitialize={true}
            initialValues={{ description, targetDate }}
            onSubmit={this.onSubmit}
            validate={this.validate}
            validateOnBlur={false}
            validateOnChange={false}
          >
            {
              (props) => (
                <Form>
                  <ErrorMessage
                    name="description"
                    component="div"
                    className="alert alert-warning"
                  />
                  <ErrorMessage
                    name="targetDate"
                    component="div"
                    className="alert alert-warning"
                  />
                  <fieldset className="form-group">
                    <label>Description</label>
                    <Field className="form-control" type="text" name="description" />
                  </fieldset>
                  <fieldset className="form-group">
                    <label>Target Date</label>
                    <Field className="form-control" type="date" name="targetDate" />
                  </fieldset>
                  <button className="btn btn-success" type="submit">Save</button>
                </Form>
              )
            }
          </Formik>
        </div>

      </div>
    )
  }
}

export default TodoComponent;