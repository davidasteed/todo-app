import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import HelloWorldDataService from '../../api/todo/HelloWorldDataService';

class WelcomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      welcomeMessage: ""
    };

    this.retrieveWelcomeMessage = this.retrieveWelcomeMessage.bind(this); // not needed with arrow functions
    this.handleSuccessfulResponse = this.handleSuccessfulResponse.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  render() {
    return (
      <>
        <h1>Welcome!</h1>
        <div className="container">
          {/* using an <a href="/todos">here</a> tag here instead of <Link> means that the entire page would be refreshed.
        Which would be inefficent for an SPA.  Use of a <Link> means that only that specific portion of a page
        will be replaced by the component being routed to*/}
          Welcome {this.props.match.params.name}.  You can manage your todos <Link to="/todos">here</Link>
        </div>
        <div className="container">
          You can run the counter app <Link to="/counter">here</Link>
        </div>
        <div className="container">
          Click here to get a customized welcome message.
          <button onClick={this.retrieveWelcomeMessage} className="btn btn-success">Get Welcome Message</button>
        </div>
        <div className="container">
          {this.state.welcomeMessage ? this.state.welcomeMessage : ""}
        </div>
      </>
    )
  }

  retrieveWelcomeMessage = () => {
    HelloWorldDataService.executeHelloWorldPathVariableService(this.props.match.params.name)
      .then((response) => {
        this.handleSuccessfulResponse(response);
      })
      .catch((error) => {
        this.handleError(error);
      });
  };

  handleSuccessfulResponse = (response) => {
    this.setState({ welcomeMessage: response.data.message });
  };

  handleError = (error) => {
    // there is an edge case when i click on the "Get Welcome Message"
    // the check for "this.welcomeMessage" is required when the backend is down and network error is returning:
    // GET http://localhost:8080/hello-world/path-variable/in28minutes,%20dummy net::ERR_CONNECTION_REFUSED

    // and the browser console shows:
    // Unhandled Rejection (TypeError): Cannot read properties of undefined (reading 'data')
    //   | ^  71 |     this.setState({ welcomeMessage: error.response.data.message });
    //      > 47 |     this.handleError(error);
    //  (*this* object is highlighted in red)

    // Perhaps the workaround has something to do with:
    //    on the "first render", state.welcomeMessage is an empty string,
    //    so state.welcomeMessage.data is defined
    //    ** only later are you updating the state.welcomMessage to an object
    // also:  somehow:  the "this" object gets equated to the "state" object

    // perhaps the workaround for the above edge case is do retrieveWelcomeMessage() in componentDidMount()
    // if (this.welcomeMessage) {
    //   console.log(error.response.data.error, error.response.data.message);
    //   this.setState({ welcomeMessage: error.response.data.message });
    // }

    console.log(error.response);

    let errorMessage = "";
    if (error.message) {
      errorMessage += error.message;
    }

    if (error.response && error.response.data) {
      errorMessage += error.response.data.message;
    }

    this.setState({ welcomeMessage: errorMessage });
  };

}



export default WelcomeComponent;