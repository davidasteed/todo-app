import React, { Component } from 'react';

class LogoutComponent extends Component {
  render() {
    return (
      <>
        <h1>You are logged out</h1>
        <div className="container">
          <span>Thank you for using our application</span>
        </div>
      </>
    )
  }
}

export default LogoutComponent;