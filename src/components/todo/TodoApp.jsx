import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AuthenticatedRoute from "./AuthenticatedRoute.jsx";
import Counter from "../counter/Counter.jsx";
import ErrorComponent from "./ErrorComponent.jsx";
import FooterComponent from "./FooterComponent.jsx";
import HeaderComponent from "./HeaderComponent.jsx";
import ListTodosComponent from "./ListTodosComponent";
import LoginComponent from "./LoginComponent.jsx";
import LogoutComponent from "./LogoutComponent.jsx";
import TodoComponent from "./TodoComponent.jsx";
import WelcomeComponent from "./WelcomeComponent.jsx";

class TodoApp extends Component {
  render() {
    return (
      <div className="TodoApp">
        <Router>
          <>
            <HeaderComponent />
            <Switch>
              {/* order of precedence can be used or "exact" keyword */}
              <Route path="/" exact component={LoginComponent} />
              <Route path="/login" component={LoginComponent} />
              <AuthenticatedRoute path="/welcome/:name" component={WelcomeComponent} />
              <AuthenticatedRoute path="/todos/:id" component={TodoComponent} />
              <AuthenticatedRoute path="/todos" component={ListTodosComponent} />
              <AuthenticatedRoute path="/logout" component={LogoutComponent} />
              <AuthenticatedRoute path="/counter" component={Counter} />

              {/* default route */}
              <Route component={ErrorComponent} />

            </Switch>
            <FooterComponent />
          </>
        </Router>

        {/* <LoginComponent /> */}
      </div>
    )
  }
}

export default TodoApp;